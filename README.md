<p align="center">
  <img src="readme.png">
</p>

# Svale UI

Component library for building fast & beautiful applications on the web. Made with Svelte

## Developing

Install dependencies with `npm install` (or `pnpm install` or `yarn`).

Start a development server:

```bash
npm run dev
```

## Building

To create a production version of the app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy the app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

---

Built in 🇳🇴 with [SvelteKit](https://kit.svelte.dev/)
