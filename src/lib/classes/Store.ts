import { type Writable, writable, get as getFromWriteable } from 'svelte/store';

export interface StoreConstructorOptions {
	storeName: string;
	defaultStateValue: any;
}

export interface StoreChildConstructorOptions {
	defaultState: any;
}

export interface State {
	value: any;
	useDefault: boolean;
}

export abstract class Store {
	public liveValue;
	public storeName: string;

	protected state: Writable<State>;

	private defaultStateValue;

	constructor(options: StoreConstructorOptions) {
		this.storeName = options.storeName;

		this.state = writable<State>(
			this.getInitialState({
				value: options.defaultStateValue,
				useDefault: true
			})
		);

		this.setupPersistance();

		this.defaultStateValue = options.defaultStateValue;

		this.liveValue = {
			subscribe: this.state.subscribe
		};
	}

	public get(): State {
		return getFromWriteable(this.state);
	}

	public set(newValue: any): void {
		return this.state.set({
			value: newValue,
			useDefault: false
		});
	}

	public reset(): void {
		return this.state.set({
			value: this.defaultStateValue,
			useDefault: true
		});
	}

	protected setupPersistance(): void {
		this.state.subscribe((value) => {
			window.localStorage.setItem(this.storeName, JSON.stringify(value));
		});
	}

	protected getInitialState(defaultValue: State): State {
		let storedValue = defaultValue;

		try {
			storedValue = JSON.parse(window.localStorage.getItem(this.storeName));
		} catch (error) {
			console.error("Couldn't parsed the stored value");
		}

		if (storedValue && storedValue.useDefault) {
			return defaultValue;
		}

		return storedValue ?? defaultValue;
	}
}
